import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    routerName:'storecheck',
    sideIndex:'1-1'
  },
  mutations: {
    changeRouterName(state,name) {
      state.routerName = name
    },
    changeSideIndex(state,name) {
      state.sideIndex = name
    }
  },
  actions: {
  },
  modules: {
  }
})
