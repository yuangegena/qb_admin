import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

// 禁止全局打印
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect:'/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/login.vue'),
    meta:{title:'登录'}
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('@/views/index/index.vue'),
    redirect:'/storecheck',
    children:[
      {
        path: '/storecheck',
        name: 'storecheck',
        component: () => import('@/views/checkmanagement/storecheck.vue'),
        meta:{title:'商家审核'},
        children:[
          
        ]
      },
      {
        path: '/reject',
        name: 'reject',
        component: () => import('@/views/checkmanagement/storemain/reject.vue')
      },
      {
        path: '/storedetail',
        name: 'storedetail',
        component: () => import('@/views/checkmanagement/storemain/storedetail.vue')
      },
      {
        path: '/setdetail',
        name: 'setdetail',
        component: () => import('@/views/checkmanagement/storemain/setdetail.vue')
      },
      {
        path: '/blacklist',
        name: 'blacklist',
        component: () => import('@/views/checkmanagement/blacklist.vue'),
        meta:{title:'商家黑名单'},
      },
      {
        path: '/datastatistics',
        name: 'datastatistics',
        component: () => import('@/views/storemanagement/datastatistics.vue'),
        meta:{title:'数据统计'},
      },
      {
        path: '/datadetail',
        name: 'datadetail',
        component: () => import('@/views/storemanagement/datamain/datadetail.vue'),
        meta:{title:'商家详情'},
      },
      {
        path: '/discount',
        name: 'discount',
        component: () => import('@/views/storemanagement/discount.vue'),
        meta:{title:'优惠券管理'},
      },
      {
        path: '/bill',
        name: 'bill',
        component: () => import('@/views/billmanagement/bill.vue'),
        meta:{title:'账单管理'},
      },
      {
        path: '/billdetail',
        name: 'billdetail',
        component: () => import('@/views/billmanagement/billmain/billdetail.vue'),
        meta:{title:'账单管理'},
      },
      {
        path: '/mark',
        name: 'mark',
        component: () => import('@/views/markmanagement/mark.vue')
      },
    ]
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
